package com.adventofcode

interface TestWithInput {
    val Any.actualInput: List<String>
        get() = readInput()
}